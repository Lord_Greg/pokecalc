﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Pokecalc.Models;

namespace Pokecalc
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();

            setPokemonMaxLevel();
        }


        #region Events

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            Pokemon pokemon = new Pokemon(Convert.ToDouble(nudPokemonLevel.Value));

            switch (tcMode.SelectedTab.Name.ToString())
            {
                case "tpPowerUpCalc":
                    Dictionary<string, int> powerUpCosts = Calculations.powerUpCosts(pokemon.Level, Convert.ToInt32(nudTrainerLevel.Value));
                    lblPowerUp_CandyCost.Text = powerUpCosts["candy"].ToString();
                    lblPowerUp_StardustCost.Text = powerUpCosts["stardust"].ToString();
                    break;
            }
        }


        private void nudTrainerLevel_ValueChanged(object sender, EventArgs e)
        {
            setPokemonMaxLevel();
        }

        #endregion
        

        private void setPokemonMaxLevel()
        {
            nudPokemonLevel.Maximum = Convert.ToDecimal(Convert.ToDouble(nudTrainerLevel.Value) + 0.5);
        }
    }
}
