﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokecalc.Models
{
    class Level
    {
        #region Variables

        private double number;

        #endregion

        #region Constructors

        public Level(double level = 1)
        {
            Number = level;
        }

        public Level(int level = 1)
        {
            Number = Convert.ToDouble(level);
        }

        #endregion

        #region Methods

        public void up()
        {
            number += 0.5;
        }

        #endregion

        #region Properties

        public double Number
        {
            get
            {
                return number;
            }

            set
            {
                number = value;
            }
        }

        public int Candy
        {
            get
            {
                double level = Number;

                if (level < 11) return 1;
                else if (level < 21) return 2;
                else if (level < 31) return 3;
                else return 4;
            }
        }

        public int Stardust
        {
            get
            {
                double level = Number;

                if (level < 3) return 200;
                else if (level < 4) return 400;
                else if (level < 7) return 600;
                else if (level < 9) return 800;
                else if (level < 11) return 1000;
                else if (level < 13) return 1300;
                else if (level < 15) return 1600;
                else if (level < 17) return 1900;
                else if (level < 19) return 2200;
                else if (level < 21) return 2500;
                else if (level < 23) return 3000;
                else if (level < 25) return 3500;
                else if (level < 27) return 4000;
                else if (level < 29) return 4500;
                else if (level < 31) return 5000;
                else if (level < 33) return 6000;
                else if (level < 35) return 7000;
                else if (level < 37) return 8000;
                else if (level < 39) return 9000;
                else return 10000;
            }
        }

        #endregion
    }
}
