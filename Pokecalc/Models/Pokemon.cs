﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokecalc.Models
{
    class Pokemon
    {
        #region Variables

        private string name;
        private Level level;
        private int cp;

        #endregion

        #region Constructors

        public Pokemon(double level)
        {
            this.level = new Level(level);
        }

        #endregion

        #region Methods



        #endregion

        #region Properties

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        internal Level Level
        {
            get
            {
                return level;
            }

            set
            {
                level = new Level(Convert.ToDouble(value));
            }
        }

        public int CP
        {
            get
            {
                return cp;
            }

            set
            {
                cp = value;
            }
        }

        #endregion
    }
}
