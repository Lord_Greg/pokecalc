﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pokecalc.Models;

namespace Pokecalc
{
    class Calculations
    {
        public static Dictionary<string, int> powerUpCosts(Level currentLevel, int targetLevel)
        {
            int stardust = 0, candy = 0;

            while(currentLevel.Number <= targetLevel)
            {
                stardust += currentLevel.Stardust;
                candy += currentLevel.Candy;

                currentLevel.up();
            }

            Dictionary<string, int> costs = new Dictionary<string, int>();
            costs.Add("stardust", stardust);
            costs.Add("candy", candy);

            return costs;
        }
    }
}
