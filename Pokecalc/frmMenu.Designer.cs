﻿namespace Pokecalc
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tcMode = new System.Windows.Forms.TabControl();
            this.tpPowerUpCalc = new System.Windows.Forms.TabPage();
            this.nudTrainerLevel = new System.Windows.Forms.NumericUpDown();
            this.lblTrainerLevel = new System.Windows.Forms.Label();
            this.flpTrainerLevel = new System.Windows.Forms.FlowLayoutPanel();
            this.flpPokemonLevel = new System.Windows.Forms.FlowLayoutPanel();
            this.lblPokemonLevel = new System.Windows.Forms.Label();
            this.nudPokemonLevel = new System.Windows.Forms.NumericUpDown();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.lblPowerUp_CandyCost = new System.Windows.Forms.Label();
            this.lblPowerUp_TextStardustCost = new System.Windows.Forms.Label();
            this.lblPowerUp_StardustCost = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblPowerUp_TextCandyCost = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tcMode.SuspendLayout();
            this.tpPowerUpCalc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTrainerLevel)).BeginInit();
            this.flpTrainerLevel.SuspendLayout();
            this.flpPokemonLevel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPokemonLevel)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcMode
            // 
            this.tcMode.Controls.Add(this.tpPowerUpCalc);
            this.tcMode.Location = new System.Drawing.Point(12, 64);
            this.tcMode.Name = "tcMode";
            this.tcMode.SelectedIndex = 0;
            this.tcMode.Size = new System.Drawing.Size(560, 285);
            this.tcMode.TabIndex = 0;
            // 
            // tpPowerUpCalc
            // 
            this.tpPowerUpCalc.Controls.Add(this.label1);
            this.tpPowerUpCalc.Controls.Add(this.panel1);
            this.tpPowerUpCalc.Location = new System.Drawing.Point(4, 22);
            this.tpPowerUpCalc.Name = "tpPowerUpCalc";
            this.tpPowerUpCalc.Padding = new System.Windows.Forms.Padding(3);
            this.tpPowerUpCalc.Size = new System.Drawing.Size(552, 259);
            this.tpPowerUpCalc.TabIndex = 0;
            this.tpPowerUpCalc.Text = "Koszt Power-up\'ów";
            this.tpPowerUpCalc.UseVisualStyleBackColor = true;
            // 
            // nudTrainerLevel
            // 
            this.nudTrainerLevel.Location = new System.Drawing.Point(3, 16);
            this.nudTrainerLevel.Maximum = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.nudTrainerLevel.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudTrainerLevel.Name = "nudTrainerLevel";
            this.nudTrainerLevel.Size = new System.Drawing.Size(54, 20);
            this.nudTrainerLevel.TabIndex = 1;
            this.nudTrainerLevel.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudTrainerLevel.ValueChanged += new System.EventHandler(this.nudTrainerLevel_ValueChanged);
            // 
            // lblTrainerLevel
            // 
            this.lblTrainerLevel.AutoSize = true;
            this.lblTrainerLevel.Location = new System.Drawing.Point(3, 0);
            this.lblTrainerLevel.Name = "lblTrainerLevel";
            this.lblTrainerLevel.Size = new System.Drawing.Size(81, 13);
            this.lblTrainerLevel.TabIndex = 2;
            this.lblTrainerLevel.Text = "Poziom Trenera";
            // 
            // flpTrainerLevel
            // 
            this.flpTrainerLevel.Controls.Add(this.lblTrainerLevel);
            this.flpTrainerLevel.Controls.Add(this.nudTrainerLevel);
            this.flpTrainerLevel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpTrainerLevel.Location = new System.Drawing.Point(16, 12);
            this.flpTrainerLevel.Name = "flpTrainerLevel";
            this.flpTrainerLevel.Size = new System.Drawing.Size(110, 46);
            this.flpTrainerLevel.TabIndex = 3;
            // 
            // flpPokemonLevel
            // 
            this.flpPokemonLevel.Controls.Add(this.lblPokemonLevel);
            this.flpPokemonLevel.Controls.Add(this.nudPokemonLevel);
            this.flpPokemonLevel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpPokemonLevel.Location = new System.Drawing.Point(149, 12);
            this.flpPokemonLevel.Name = "flpPokemonLevel";
            this.flpPokemonLevel.Size = new System.Drawing.Size(110, 46);
            this.flpPokemonLevel.TabIndex = 4;
            // 
            // lblPokemonLevel
            // 
            this.lblPokemonLevel.AutoSize = true;
            this.lblPokemonLevel.Location = new System.Drawing.Point(3, 0);
            this.lblPokemonLevel.Name = "lblPokemonLevel";
            this.lblPokemonLevel.Size = new System.Drawing.Size(95, 13);
            this.lblPokemonLevel.TabIndex = 2;
            this.lblPokemonLevel.Text = "Poziom Pokemona";
            // 
            // nudPokemonLevel
            // 
            this.nudPokemonLevel.DecimalPlaces = 1;
            this.nudPokemonLevel.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.nudPokemonLevel.Location = new System.Drawing.Point(3, 16);
            this.nudPokemonLevel.Maximum = new decimal(new int[] {
            405,
            0,
            0,
            65536});
            this.nudPokemonLevel.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudPokemonLevel.Name = "nudPokemonLevel";
            this.nudPokemonLevel.Size = new System.Drawing.Size(54, 20);
            this.nudPokemonLevel.TabIndex = 1;
            this.nudPokemonLevel.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnCalculate
            // 
            this.btnCalculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnCalculate.Location = new System.Drawing.Point(490, 23);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(78, 30);
            this.btnCalculate.TabIndex = 5;
            this.btnCalculate.Text = "Oblicz!";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // lblPowerUp_CandyCost
            // 
            this.lblPowerUp_CandyCost.AutoSize = true;
            this.lblPowerUp_CandyCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPowerUp_CandyCost.Location = new System.Drawing.Point(168, 3);
            this.lblPowerUp_CandyCost.Name = "lblPowerUp_CandyCost";
            this.lblPowerUp_CandyCost.Size = new System.Drawing.Size(24, 25);
            this.lblPowerUp_CandyCost.TabIndex = 1;
            this.lblPowerUp_CandyCost.Text = "0";
            // 
            // lblPowerUp_TextStardustCost
            // 
            this.lblPowerUp_TextStardustCost.AutoSize = true;
            this.lblPowerUp_TextStardustCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPowerUp_TextStardustCost.Location = new System.Drawing.Point(3, 42);
            this.lblPowerUp_TextStardustCost.Name = "lblPowerUp_TextStardustCost";
            this.lblPowerUp_TextStardustCost.Size = new System.Drawing.Size(157, 25);
            this.lblPowerUp_TextStardustCost.TabIndex = 0;
            this.lblPowerUp_TextStardustCost.Text = "Koszt Stardustu:";
            // 
            // lblPowerUp_StardustCost
            // 
            this.lblPowerUp_StardustCost.AutoSize = true;
            this.lblPowerUp_StardustCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPowerUp_StardustCost.Location = new System.Drawing.Point(168, 42);
            this.lblPowerUp_StardustCost.Name = "lblPowerUp_StardustCost";
            this.lblPowerUp_StardustCost.Size = new System.Drawing.Size(24, 25);
            this.lblPowerUp_StardustCost.TabIndex = 1;
            this.lblPowerUp_StardustCost.Text = "0";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblPowerUp_TextStardustCost);
            this.panel1.Controls.Add(this.lblPowerUp_TextCandyCost);
            this.panel1.Controls.Add(this.lblPowerUp_StardustCost);
            this.panel1.Controls.Add(this.lblPowerUp_CandyCost);
            this.panel1.Location = new System.Drawing.Point(133, 110);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(268, 79);
            this.panel1.TabIndex = 3;
            // 
            // lblPowerUp_TextCandyCost
            // 
            this.lblPowerUp_TextCandyCost.AutoSize = true;
            this.lblPowerUp_TextCandyCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPowerUp_TextCandyCost.Location = new System.Drawing.Point(3, 3);
            this.lblPowerUp_TextCandyCost.Name = "lblPowerUp_TextCandyCost";
            this.lblPowerUp_TextCandyCost.Size = new System.Drawing.Size(165, 25);
            this.lblPowerUp_TextCandyCost.TabIndex = 0;
            this.lblPowerUp_TextCandyCost.Text = "Koszt Cukierków:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(65, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(415, 29);
            this.label1.TabIndex = 4;
            this.label1.Text = "Koszta Wymaksowania Pokemona:";
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 361);
            this.Controls.Add(this.btnCalculate);
            this.Controls.Add(this.flpPokemonLevel);
            this.Controls.Add(this.flpTrainerLevel);
            this.Controls.Add(this.tcMode);
            this.Name = "frmMenu";
            this.Text = "Pokecalc";
            this.tcMode.ResumeLayout(false);
            this.tpPowerUpCalc.ResumeLayout(false);
            this.tpPowerUpCalc.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTrainerLevel)).EndInit();
            this.flpTrainerLevel.ResumeLayout(false);
            this.flpTrainerLevel.PerformLayout();
            this.flpPokemonLevel.ResumeLayout(false);
            this.flpPokemonLevel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPokemonLevel)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcMode;
        private System.Windows.Forms.TabPage tpPowerUpCalc;
        private System.Windows.Forms.NumericUpDown nudTrainerLevel;
        private System.Windows.Forms.Label lblTrainerLevel;
        private System.Windows.Forms.FlowLayoutPanel flpTrainerLevel;
        private System.Windows.Forms.FlowLayoutPanel flpPokemonLevel;
        private System.Windows.Forms.Label lblPokemonLevel;
        private System.Windows.Forms.NumericUpDown nudPokemonLevel;
        private System.Windows.Forms.Label lblPowerUp_CandyCost;
        private System.Windows.Forms.Label lblPowerUp_TextStardustCost;
        private System.Windows.Forms.Label lblPowerUp_StardustCost;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblPowerUp_TextCandyCost;
        private System.Windows.Forms.Label label1;
    }
}

